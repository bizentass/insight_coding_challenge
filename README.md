##Insight Data Engineering - Coding Challenge 

#####Submitted by: Lalith Vikram Natarajan - lalith.natarajan@yahoo.com, lalithvi@buffalo.edu

The file <i>AntiFraud.java</i> creates graphs, unit tests the implementation and drives the fraud detection system. The file <i>TransactionGraph.java</i> has the graph class information and graph traversal methods. The batch_payment.txt and stream_payment.txt are very small subsets of the input files provided. They are primarily for testing the system on checkout. <i>run.sh</i> can be used to start the system, it includes the input and output files as arguments automatically.

├── README.md 
├── run.sh
├── src
│  	└── AntiFraud.java
│  	└── TransactionGraph.java
├── paymo_input
│   └── batch_payment.txt
|   └── stream_payment.txt
├── paymo_output
│   └── output1.txt
|   └── output2.txt
|   └── output3.txt
└── insight_testsuite
 	   ├── run_tests.sh
	   └── tests
        	└── test-1-paymo-trans
        	  	├── paymo_input
    			│   └── batch_payment.txt
    			│   └── stream_payment.txt
    			└── paymo_output
    			    └── output1.txt
    			    └── output2.txt
    			    └── output3.txt


Read https://github.com/InsightDataScience/digital-wallet/blob/master/README.md for more information regarding the challenge.