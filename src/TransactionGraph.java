import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;

/**
 * This is a graph object that maps transactions between users. Users are represented as vertices/nodes.
 * Undirected and un-weighted edges represent transactions.
 * For fast lookup we maintain the nodes in a HashMap with user id as the key and the node as the value.
 */

class TransactionGraph {
	private Map<String, List<Vertex>> adjacencyList;
	private Map<String, Vertex> vertices;

	public Map<String, Vertex> getVertices() {
		return vertices;
	}

	public void setVertices(Map<String, Vertex> vertices) {
		this.vertices = vertices;
	}

	public TransactionGraph() {
		this.adjacencyList = new HashMap<>();
		vertices = new HashMap<String, Vertex>();
	}
	
	
	/**
	 * Class to represent users as vertices in the transaction graph
	 */

	static class Vertex {
		String name;
		
		public Vertex(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
		
		@Override
		public int hashCode() {
			return this.getName().hashCode();
		}
		
		@Override
	    public boolean equals(Object obj) {
	        if (obj == this) {
	            return true;
	        }
	        if (obj == null || obj.getClass() != this.getClass()) {
	            return false;
	        }

	        Vertex newObj = (Vertex) obj;
	        return name == newObj.name && (name == newObj.name || (name != null && name.equals(newObj.getName())));
		}

	}

	/**
	 * Add a new neighbor vertex to any given user vertex.
	 * 
	 * @param recUser
	 * @param destNode
	 */
	
	private void addEdge(String recUser, Vertex destNode) {
		List<Vertex> neighboringVertices = adjacencyList.get(recUser);
		if (neighboringVertices == null || neighboringVertices.isEmpty()) { 
			// Inserting first node into adjacency list
			neighboringVertices = new ArrayList<Vertex>();
			neighboringVertices.add(destNode);
		} else {
			neighboringVertices.add(destNode);
		}
		adjacencyList.put(recUser, neighboringVertices);
	}

	/**
	 * Method that adds an edge between two users, supports undirected and directed edges
	 * 
	 * @param sendingUser
	 * @param receivingUser
	 */
	
	public void addEdge(String sendingUser, String receivingUser, boolean directed) {
		Objects.requireNonNull(sendingUser);
		Objects.requireNonNull(receivingUser);
		
		this.addEdge(sendingUser, new Vertex(receivingUser));
		if (!directed) // if it is not directed, make sure adjacency list of 'y' is also present in 'x'
	            addEdge(receivingUser, sendingUser, true);
	    else {
	    	this.vertices.put(sendingUser, new Vertex(sendingUser));
	 		this.vertices.put(receivingUser, new Vertex(receivingUser));
	     }
	 }


	/**
	 * Method that returns all vertices of the graph
	 * 
	 * @return List<Vertex>
	 */
	
	public List<Vertex> getAllVertices() {
		return new ArrayList<Vertex>(this.vertices.values());
	}

	/**
	 * Method that returns transaction graph as a string for printing and verification
	 */

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Transactions :\n");
		for (String v : this.adjacencyList.keySet()) {
			List<Vertex> neighbour = this.adjacencyList.get(v);
			for (Vertex vertex : neighbour) {
				sb.append(v + " --- " + vertex.getName() + "\n");
			}
		}
		sb.append("Users :" + this.getAllVertices());
		return sb.toString();
	}
	
	/**
	 * Method that carries out a bidirectional breadth first search and returns if a vertex is present in the 
	 * adjacency lists of it or it's neighbors limited to a level/degree. 
	 * Normal breadth first search holds good only for smaller graphs.
	 * 
	 * @param graph
	 * @param sender
	 * @param receiver
	 * @param degree
	 */
	
	public static boolean biDirectionalBFS(TransactionGraph graph, Vertex sender, Vertex receiver, int degree) {

		if(sender == null || receiver == null || degree < 1) {
			return false;
		}
		
		Queue<Vertex> senderQ = new ArrayDeque<Vertex>();
	    Queue<Vertex> receiverQ = new ArrayDeque<Vertex>();
	    Set<Vertex> senderVisited = new HashSet<Vertex>();
	    Set<Vertex> receiverVisited = new HashSet<Vertex>();
	    
	    // Add sender and receiver to both visited set and queues
	    senderQ.add(sender);
	    receiverQ.add(receiver);
	    
	    senderVisited.add(sender);
	    receiverVisited.add(receiver);
	    
	    // Both the queues have to be iterated in parallel arriving at a result somewhere in between
	    while (!senderQ.isEmpty() && !receiverQ.isEmpty()) {
	    	if(--degree >= 0 ) {
		        if (helper(graph, senderQ, senderVisited, receiverVisited)) {
		          return true;
		        }
	    	} else
	    		break;
	    	
	    	if(--degree >= 0 ) {
		        if (helper(graph, receiverQ, receiverVisited, senderVisited)) {
		          return true;
		        }
	    	} else
	    		break;
	      }
	    
	    return false;
	}
	
	/**
	 * Helper method that swaps receiver and sender collections
	 * 
	 * @param graph
	 * @param queue
	 * @param startVertices
	 * @param endVertices
	 */
	
	private static boolean helper(TransactionGraph graph, Queue<Vertex> queue, Set<Vertex> startVertices, Set<Vertex> endVertices) {
		if (!queue.isEmpty()) {
	      Vertex current = queue.remove();
	      for (Vertex adjacent : graph.adjacencyList.get(current.name)) {
	    	  if (endVertices.contains(adjacent)) {
	        	return true;
	    	  } else if (startVertices.add(adjacent)) {
	        	queue.add(adjacent);
	    	  }
		   }
		}
		
		 return false;
	}
}
