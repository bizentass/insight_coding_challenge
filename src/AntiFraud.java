/**
 *  Program that detects suspicious transactions - fraud detection algorithm 
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AntiFraud {
	
	static List<String> readTransactions(String file) {
		
		BufferedReader br = null;
		String line = "";
		List<String> transactions = new ArrayList<String>();
		try {
		    br = new BufferedReader(new FileReader(file));
			@SuppressWarnings("unused")
			String header = br.readLine();
		    while ((line = br.readLine()) != null) {
		        String[] info = line.split(",");
		        if(info.length > 1)
		        	transactions.add(info[1].trim()+"_"+info[2].trim());
		    }
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		} finally {
		    if (br != null) {
		        try {
		            br.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}
		
		return transactions;
	}
	
	static List<String> readTransactionswithTime(String file) {
		
		BufferedReader br = null;
		String line = "";
		List<String> transactions = new ArrayList<String>();
		try {
		    br = new BufferedReader(new FileReader(file));
			@SuppressWarnings("unused")
			String header = br.readLine();
		    while ((line = br.readLine()) != null) {
		        String[] info = line.split(",");
		        if(info.length > 1)
		        	transactions.add(info[0]+"_"+info[1].trim()+"_"+info[2].trim());
		    }
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		} finally {
		    if (br != null) {
		        try {
		            br.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}
		
		return transactions;
	}
	
	/**
	 * Method to unit test the features 
	 * To enable, uncomment the calling portion in main method
	 */

	public static void unitTest() {
				
		TransactionGraph graph = new TransactionGraph();
		
		graph.addEdge("1", "2", false);
		graph.addEdge("2", "3", false);
		graph.addEdge("2", "4", false);
		graph.addEdge("4", "5", false);
		graph.addEdge("4", "6", false);
		graph.addEdge("5", "6", false);
		graph.addEdge("6", "3", false);
		graph.addEdge("6", "7", false);
		graph.addEdge("7", "8", false);

		System.out.println(graph.toString());
		ArrayList<String> results = new ArrayList<String>();
		
		// Basic degree access tests
		if (TransactionGraph.biDirectionalBFS(graph,new TransactionGraph.Vertex("1"), new TransactionGraph.Vertex("2"), 1)) 
			results.add("Passed - 1");
		else
			results.add("Failed - 1");
		
		if (TransactionGraph.biDirectionalBFS(graph,new TransactionGraph.Vertex("1"), new TransactionGraph.Vertex("3"), 2)) 
			results.add("Passed - 2");
		else
			results.add("Failed - 2");
		
		if (TransactionGraph.biDirectionalBFS(graph,new TransactionGraph.Vertex("1"), new TransactionGraph.Vertex("5"), 3))
			results.add("Passed - 3");
		else
			results.add("Failed - 3");
		
		if (TransactionGraph.biDirectionalBFS(graph,new TransactionGraph.Vertex("1"), new TransactionGraph.Vertex("7"), 4))
			results.add("Passed - 4");
		else
			results.add("Failed - 4");
		
		if (TransactionGraph.biDirectionalBFS(graph,new TransactionGraph.Vertex("1"), new TransactionGraph.Vertex("8"), 5))
			results.add("Passed - 5");
		else
			results.add("Failed - 5");

		// Since 2 is in the 1st degree of connections it should also hold true for higher connections as well
		if (TransactionGraph.biDirectionalBFS(graph,new TransactionGraph.Vertex("1"), new TransactionGraph.Vertex("2"), 5))
			results.add("Passed - 6");
		else
			results.add("Failed - 6");
		
		// Negative test cases
		// 2 to 7 should not connect less than a degree of 3
		if (!TransactionGraph.biDirectionalBFS(graph,new TransactionGraph.Vertex("2"), new TransactionGraph.Vertex("7"), 2))
			results.add("Passed - 7");
		else
			results.add("Failed - 7");
		// 1 to 5 should not connect less than a degree of 3
		if (!TransactionGraph.biDirectionalBFS(graph,new TransactionGraph.Vertex("1"), new TransactionGraph.Vertex("5"), 2))
			results.add("Passed - 8");
		else
			results.add("Failed - 8");
		
		results.forEach(result ->  System.out.println(result));
	}

	/** 
	 * Driver method to start the detection
	 * args[0] - training transactions file
	 * args[1] - testing transactions file
	 * args[2] - feature 1 output file
	 * args[3] - feature 2 output file
	 * args[4] - feature 3 output file
	 * 
	 * @param args
	 * @throws IOException
	 */
	
	public static void main(String[] args) throws IOException {
		
		// For unit testing smaller transaction graphs, uncomment the line below
		//unitTest();
		
		String trainingFile = args[0];
		
		/* Build the transaction graph using arg[0]  */
		List<String> trainingTransactions =  readTransactions(trainingFile);
		String[] transactionParties = new String[2];
		TransactionGraph graph = new TransactionGraph();
		for(String transaction: trainingTransactions) {
			transactionParties = transaction.split("_");
			graph.addEdge(transactionParties[0],transactionParties[1], false);
		}
		
		/* Test it with arg[1] file
		 * transactionParties[0] - user who wants to send money, 
		 * transactionParties[1] - user who wants to receive money
		 */
		String testingFile = args[1];
		List<String> testTransactions =  readTransactions(testingFile);
		List<String> lines = new ArrayList<String>();
		
		/* 
		 * Feature 1 - 1st degree friends
		 * If the receiver has not made a transaction with sender yet, we categorize it as unverified
		 */
		lines = new ArrayList<String>();
		for(String transaction: testTransactions) {
			transactionParties = transaction.split("_");
			TransactionGraph.Vertex senderNode = graph.getVertices().get(transactionParties[0]);
			TransactionGraph.Vertex receiverNode = graph.getVertices().get(transactionParties[1]);
			
			// 1 here specifies the degree
			if(TransactionGraph.biDirectionalBFS(graph, senderNode, receiverNode, 1))
				lines.add("trusted");
			else
				lines.add("unverified");
		}
		Path outputFile1 = Paths.get(args[2]);
		Files.write(outputFile1, lines);
				
		/* 
		 * Feature 2 - 2nd degree friends
		 * If the receiver has not made a transaction with sender yet, 
		 * but has made a transaction with friend of a sender (2nd degree of transaction graph)
		 * he/she is trusted else he/she is unverified.
		 */
		lines = new ArrayList<String>();
		for(String transaction: testTransactions) {
			transactionParties = transaction.split("_");
			TransactionGraph.Vertex sender = graph.getVertices().get(transactionParties[0]);
			TransactionGraph.Vertex receiver = graph.getVertices().get(transactionParties[1]);
			
			// 2 here specifies the degree
			if(TransactionGraph.biDirectionalBFS(graph, sender, receiver, 2))
				lines.add("trusted");
			else
				lines.add("unverified");
		}
		Path outputFile2 = Paths.get(args[3]);
		Files.write(outputFile2, lines);
		
		/* 
		 * Feature 3 -3rd degree friends
		 * If the receiver has not made a transaction with sender yet, 
		 * but has made a transaction within the 4th degree of the transaction graph of a sender,
		 * he/she is trusted else he/she is unverified.
		 */
		lines = new ArrayList<String>();
		for(String transaction: testTransactions) {
			transactionParties = transaction.split("_");
			TransactionGraph.Vertex sender = graph.getVertices().get(transactionParties[0]);    
			TransactionGraph.Vertex receiver = graph.getVertices().get(transactionParties[1]);
			
			// 4 here specifies the degree
			if(TransactionGraph.biDirectionalBFS(graph, sender, receiver, 4))
				lines.add("trusted");
			else
				lines.add("unverified");
		}
		Path outputFile3 = Paths.get(args[4]);
		Files.write(outputFile3, lines);	
		
		/* 
		 * Feature 4 - Frequent transfers
		 * If the receiver makes an unusually quick number of transfers, we block them after a threshold, 
		 * this is usually a kind of fraud.
		 * 
		 */
		
		/* Following lines are commented as the code has not been tested. This is meant for the engineer who reads the code :)
		 
		lines = new ArrayList<String>();
		List<String> testTransactionsWithTime =  readTransactionswithTime(testingFile);
		HashMap<String, Integer> userSuccessiveCountsMap = new HashMap<String, Integer>();
		HashMap<String, StreamingUserData> userMap = new HashMap<String, StreamingUserData>();
		for(String transaction: testTransactionsWithTime) {
			String[] transactionString = transaction.split("_");
			String timeStamp  = transactionString[0];
			String senderString  = transactionString[1];
	        Instant instant = Instant.parse(timeStamp);
	        if(userMap.containsKey(senderString)) {
	        	StreamingUserData sender = userMap.get(senderString);
	            if(Duration.between(sender.getTimeStamp(), instant).toMinutes() < 2) { // if successive time stamps are less than 2 mins.
	            	userSuccessiveCountsMap.merge(senderString, 1, (oldValue, one) -> oldValue + one);
	            }
	        } else {
	        	StreamingUserData sender = new StreamingUserData(transactionString[1],instant);
	        	userMap.put(senderString, sender);
	        }
	        if(userSuccessiveCountsMap.get(senderString) > 10)
	        	lines.add("trusted");
	        else
	        	lines.add("unverified");
	        
		}
		Path outputFile4 = Paths.get(args[5]);
		Files.write(outputFile4, lines); */
	}
}

/* class StreamingUserData {
	private String username;
	private Instant timeStamp;
	
	public StreamingUserData(String username, Instant instant) {
		this.username = username;
		this.timeStamp = instant;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public Instant getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Instant timeStamp) {
		this.timeStamp = timeStamp;
	}
} */